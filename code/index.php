<?php
// Get all URL's

$jsonStoc = file_get_contents('http://localhost:3000/stockage'); //Stockage
$jsonAlim = file_get_contents('http://localhost:3000/alimentation'); //Alimentation
$jsonBoit = file_get_contents('http://localhost:3000/boitier'); //Boitier
$jsonCM = file_get_contents('http://localhost:3000/cartemere'); //Carte Mere

$jsonCG = file_get_contents('http://localhost:3000/cartegraphique'); //Carte Graphique
$jsonRam= file_get_contents('http://localhost:3000/ram'); //RAM
$jsonVent = file_get_contents('http://localhost:3000/ventirad'); //Ventirad
$jsonProc = file_get_contents('http://localhost:3000/processeur'); //Processeur */

$ProcObj = json_decode($jsonProc);
$AlimObj = json_decode($jsonAlim);
$BoitObj = json_decode($jsonBoit);
$CMObj = json_decode($jsonCM);

$CGObj = json_decode($jsonCG);
$RamObj = json_decode($jsonRam);
$VentObj = json_decode($jsonVent);
$StocObj = json_decode($jsonStoc);

?>

<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <link rel = "stylesheet" type = "text/css" href = "style.css" />
      <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="https://kit.fontawesome.com/7b7a0d17fe.js"></script>
    <title>Joptimisme</title>
  </head>
  <body>
    <header>
      <nav class="navbar navbar-dark bg-primary navbar-expand-lg">
            <div class="container">
                <div class="row col-12">
                        <a id="home" class="navbar-brand" href="#"><i class="fas fa-laptop fa-3x"></i></a>
                        <p class="text-center">J'optimisme</p>
            </div>
        </nav>
    </header>
    <div class="container">
        <ul class="list-group">

            <li class="list-group-item list-group-item-action d-flex justify-content-between align-items-center mt-5" data-toggle="modal" data-target="#processorModal">
                <a class="modalText" href="#" >
                    + Processeur
                </a>
                <?php echo '<span class="badge badge-primary badge-pill">' . count($ProcObj) .'</span>' ?>
            </li>
            <li class="list-group-item list-group-item-action d-flex justify-content-between align-items-center mt-5" data-toggle="modal" data-target="#cartegraphiqueModal">
                <a class="modalText" href="#" >
                    + Carte graphique
                </a>
                <?php echo '<span class="badge badge-primary badge-pill">' . count($CGObj) .'</span>' ?>
            </li>
            <li class="list-group-item list-group-item-action d-flex justify-content-between align-items-center mt-5" data-toggle="modal" data-target="#stockageModal">
                <a class="modalText" href="#" >
                    + Stockage
                </a>
                <?php echo '<span class="badge badge-primary badge-pill">' . count($StocObj) .'</span>' ?>
            </li>
            <li class="list-group-item list-group-item-action d-flex justify-content-between align-items-center mt-5" data-toggle="modal" data-target="#alimentationModal">
                <a class="modalText" href="#" >
                    + Alimentation
                </a>
                <?php echo '<span class="badge badge-primary badge-pill">' . count($AlimObj) .'</span>' ?>
            </li>
            <li class="list-group-item list-group-item-action d-flex justify-content-between align-items-center mt-5"  data-toggle="modal" data-target="#boitierModal">
                <a class="modalText" href="#">
                    + Boitier
                </a>
                <?php echo '<span class="badge badge-primary badge-pill">' . count($BoitObj) .'</span>' ?>
            </li>
            <li class="list-group-item list-group-item-action d-flex justify-content-between align-items-center mt-5" data-toggle="modal" data-target="#ramModal">
                <a class="modalText" href="#" >
                    + Ram
                </a>
                <?php echo '<span class="badge badge-primary badge-pill">' . count($RamObj) .'</span>' ?>
            </li>
            <li class="list-group-item list-group-item-action d-flex justify-content-between align-items-center mt-5" data-toggle="modal" data-target="#ventiradModal">
                <a class="modalText" href="#" >
                    + Ventirad
                </a>
                <?php echo '<span class="badge badge-primary badge-pill">' . count($VentObj) .'</span>' ?>
            </li>
            <li class="list-group-item list-group-item-action d-flex justify-content-between align-items-center mt-5" data-toggle="modal" data-target="#cartemereModal">
                <a class="modalText" href="#" >
                    + Carte mère
                </a>
                <?php echo '<span class="badge badge-primary badge-pill">' . count($CMObj) .'</span>' ?>
            </li>



        <!-- Modal Processor -->
        <div class="modal fade" id="processorModal" tabindex="-1" role="dialog" aria-labelledby="processorModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-xl" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Galerie Processeur</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="modal-body">
                            <?php
                            foreach ($ProcObj as &$data) {
                                echo '<h5> ' .$data->marque.' | '.$data->modele. ' | ' .$data->socket .' | '.$data->tdp . ' TDP <span class="price"> '. $data->prix. ' euros</span></h5>';
                                echo '<hr>';
                            }
                            ?>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal -->

        <!-- Modal Ventirad -->
        <div class="modal fade" id="ventiradModal" tabindex="-1" role="dialog" aria-labelledby="ventiradModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-xl" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="ventiradModalLabel">Galerie Ventirad</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="modal-body">
                            <?php
                            foreach ($VentObj as &$data) {
                                echo '<h5> ' .$data->marque.' | '.$data->modele. ' | '.$data->tdp . ' TDP <span class="price"> '. $data->prix. ' euros</span></h5>';
                                echo '<hr>';
                            }
                            ?>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal -->

        <!-- Modal Ram -->
        <div class="modal fade" id="ramModal" tabindex="-1" role="dialog" aria-labelledby="ramModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-xl" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="ramModalLabel">Galerie RAM</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="modal-body">
                            <?php
                            foreach ($RamObj as &$data) {
                                echo '<h5> ' .$data->marque.' | '.$data->modele. ' | ' .$data->frequence .' MHz  | ' .$data->capacite .' Go | ' .$data->format .' <span class="price"> '. $data->prix. ' euros</span></h5>';
                                echo '<hr>';
                            }
                            ?>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal -->

        <!-- Modal Boitier -->
        <div class="modal fade" id="boitierModal" tabindex="-1" role="dialog" aria-labelledby="boitierModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-xl" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="boitierModalLabel">Galerie Boitier</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="modal-body">
                            <?php
                            foreach ($BoitObj as &$data) {
                                echo '<h5> ' .$data->marque.' | '.$data->modele. ' | ' .$data->hauteur .' Cm. hauteur  | ' .$data->largeur .' Cm. largeur  <span class="price"> '. $data->prix. ' euros</span></h5>';
                                echo '<hr>';
                            }
                            ?>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal -->

        <!-- Modal Carte graphique -->
        <div class="modal fade" id="cartegraphiqueModal" tabindex="-1" role="dialog" aria-labelledby="cartegraphiqueModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-xl" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="cartegraphiqueModalLabel">Galerie carte graphique</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="modal-body">
                            <?php
                            foreach ($CGObj as &$data) {
                                echo '<h5> ' .$data->marque.' | '.$data->modele. ' | ' .$data->longeur .' mm longeur  | ' .$data->largeur .' mm. largeur  <span class="price"> '. $data->prix. ' euros</span></h5>';
                                echo '<hr>';
                            }
                            ?>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal -->

        <!-- Modal Carte mère -->
        <div class="modal fade" id="cartemereModal" tabindex="-1" role="dialog" aria-labelledby="cartemereModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-xl" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="cartemereModalLabel">Galerie carte mère</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="modal-body">
                            <?php
                            foreach ($CMObj as &$data) {
                                echo '<h5> ' .$data->marque.' | '.$data->modele. ' | ' .$data->socket .'  <span class="price"> '. $data->prix. ' euros</span></h5>';
                                echo '<hr>';
                            }
                            ?>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal -->

        <!-- Modal Alimentation -->
        <div class="modal fade" id="alimentationModal" tabindex="-1" role="dialog" aria-labelledby="alimentationModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-xl" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="alimentationModalLabel">Galerie alimentation</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="modal-body">
                            <?php
                            foreach ($AlimObj as &$data) {
                                echo '<h5> ' .$data->marque.' | '.$data->modele. ' | ' .$data->puissance .' Watt  <span class="price"> '. $data->prix. ' euros</span></h5>';
                                echo '<hr>';
                            }
                            ?>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal -->

        <!-- Modal Stockage -->
        <div class="modal fade" id="stockageModal" tabindex="-1" role="dialog" aria-labelledby="stockageModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-xl" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="stockageModalLabel">Galerie stockage</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="modal-body">
                            <?php
                            foreach ($StocObj as &$data) {
                                echo '<h5> ' .$data->marque.' | '.$data->modele. ' | ' .$data->capacite .' Go | ' .$data->type .' <span class="price"> '. $data->prix. ' euros</span></h5>';
                                echo '<hr>';
                            }
                            ?>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal -->

    </div>
    <footer>
    </footer>
  </body>
</html>

